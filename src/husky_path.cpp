/*
    Arthur : Chun-Jung Lin
    Mail : chadlin.gdr07g@nctu.edu.tw
    Network Control Robotics Lab (NCRL)
*/

#include <ros/ros.h>
#include "ncrl_ugv_ctrl/husky_path.h"

std::string Path_Topic, POS_topic;
double freq;
// declare global variable
struct PID pid_x;
struct PID pid_y;

ros::Publisher pub_cmd;
ros::Publisher pub_path;

//queue<geometry_msgs::PoseStamped::ConstPtr> path;
nav_msgs::Path path;
ncrl_tf::Trans lio_pose, goal_pose;
ncrl_tf::Point err_v, err_q;
int ENABLE_FEEDFORWARD = 0;
double init_time;
bool systemInit = false;
double minCmdX, maxCmdX, minCmdW, maxCmdW;

bool readParameter(ros::NodeHandle &nh){
    bool result = true;
    // get topic name
    if (!nh.getParam("Path_topic", Path_Topic) || !nh.getParam("POS_topic", POS_topic)){
        ROS_ERROR("Failed to get param 'Path_topic'");
        ROS_ERROR("Failed to get param 'POS_topic'");
        result = false;
    }

    // get pid variable
    if (!nh.getParam("X_KP", pid_x.KP) || !nh.getParam("X_KI", pid_x.KI) || !nh.getParam("X_KD", pid_x.KD)){
        ROS_ERROR("Failed to get param x axis PID");
        result = false;
    }
    if (!nh.getParam("Y_KP", pid_y.KP) || !nh.getParam("Y_KI", pid_y.KI) || !nh.getParam("Y_KD", pid_y.KD)){
        ROS_ERROR("Failed to get param y axis PID");
        result = false;
    }

    // get frequency
    if (!nh.getParam("FREQ", freq)){
        ROS_ERROR("Failed to get param 'FREQ'");
        result = false;
    }

    if (!nh.getParam("max_vel_x", maxCmdX) || !nh.getParam("min_vel_x", minCmdX) ||
        !nh.getParam("max_vel_w", maxCmdW) || !nh.getParam("min_vel_w", minCmdW)){
        ROS_ERROR("Failed to get confinement of cmd");
        result = false;
    }

    return result;
}

void cb_pos(const nav_msgs::Odometry::ConstPtr& msg){
    Eigen::Vector3d v_(msg->pose.pose.position.x, msg->pose.pose.position.y, msg->pose.pose.position.z);
    Eigen::Quaterniond q_(msg->pose.pose.orientation.w,
                          msg->pose.pose.orientation.x,
                          msg->pose.pose.orientation.y,
                          msg->pose.pose.orientation.z);
    ncrl_tf::setTrans(lio_pose, q_, v_);
    //ncrl_tf::setTrans(goal_pose, q_, v_);
}

//void cb_pos_test(const nav_msgs::Path::ConstPtr& msg_){
//    geometry_msgs::PoseStamped msg = msg_->poses[msg_->poses.size() - 1];
//    Eigen::Vector3d v_(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z);
//    Eigen::Quaterniond q_(msg.pose.orientation.w,
//                          msg.pose.orientation.x,
//                          msg.pose.orientation.y,
//                          msg.pose.orientation.z);
//    ncrl_tf::setTrans(lio_pose, q_, v_);
//    //ncrl_tf::setTrans(goal_pose, q_, v_);
//}

void cb_path(const nav_msgs::Path::ConstPtr& msg){
    nav_msgs::Path path_ = *msg;
    if (!systemInit){
        init_time = ros::Time::now().toSec();
        systemInit = true;
    }

    if (path.poses.size() > 0){
        // match new path and old path
        double start_time = path_.poses[0].header.stamp.toSec();
        int ind = 0;
        for (; ind < path.poses.size() - 1; ind++){
            double time_ = path.poses[ind + 1].header.stamp.toSec();
            if (time_ > start_time)
                break;
        }
//        std::cout << "time : " << path_.poses[0].header.stamp.toSec()
//                  << " " << path.poses[ind].header.stamp.toSec()
//                  << " " << path.poses[ind + 1].header.stamp.toSec() << std::endl;
//        std::cout << "size path : " << path.poses.size() << " and index is " << ind << std::endl;

        // erase overlap
        path.poses.erase(path.poses.begin() + ind + 1, path.poses.end());
//        std::cout << "size path : " << path.poses.size() << std::endl;

//        std::cout << "new path : " << path_.poses.size() << std::endl;

        // add new path
        for (int i = 0; i < path_.poses.size(); i++)
            path.poses.push_back(path_.poses[i]);

//        std::cout << "size path : " << path.poses.size() << std::endl;

    } else {
        path = *msg;
    }

}

void process(){
    static tf::TransformBroadcaster br;
    ros::Rate r(freq);
    int flag = 1;
    geometry_msgs::Twist vel_cmd;

    while (ros::ok()){
        // keyboard control
        int c = getch();
        if (c != EOF){
          switch(c){
            case 49:     // key 1
              flag = 1;
            break;
            case 50:     // key 2
              flag = 2;
            break;
            case 51:     // key 3
              flag = 3;
            break;
            case 52:     // key 4
              ENABLE_FEEDFORWARD = 1 - ENABLE_FEEDFORWARD;
            break;
            case 119:    // key w
              vel_cmd.linear.x += 0.1;
            break;
            case 115:    // key s
              vel_cmd.linear.x += -0.1;
            break;
            case 100:    // key d
              vel_cmd.angular.z += -0.1;
            break;
            case 97:    // key a
              vel_cmd.angular.z += 0.1;
            break;
            case 114:    // key r
              vel_cmd.linear.x = 0;
              vel_cmd.angular.z = 0;
            break;
            case 105:    // key i
              goal_pose.v(0) += 0.1;
            break;
            case 106:    // key j
              goal_pose.v(1) += 0.1;
            break;
            case 107:    // key k
              goal_pose.v(0) -= 0.1;
            break;
            case 108:    // key l
              goal_pose.v(1) -= 0.1;
            break;
          }
        }

        if (flag == 1){
            ROS_INFO(" ===== KEYBOARD CONTROL ===== ");
            ENABLE_FEEDFORWARD = 0;
            goal_pose.v = lio_pose.v;
            goal_pose.q = lio_pose.q;
        } else if (flag == 2){
            ROS_INFO(" ===== TRACE ===== ");
        } else if (flag == 3){
            ROS_INFO(" ===== TRACE OLD ===== ");
        }

        if (ENABLE_FEEDFORWARD)
            ROS_INFO(" ===== ENABLE FEEDFORWARD TERM =====");

        if (flag == 1)
            pub_cmd.publish(vel_cmd);

        // choose suitable point
        if (path.poses.size() > 0 && flag != 1){
            int goal_ind;
            if (flag == 3){
                int size = 50;
                if (size > path.poses.size() - 5)
                    size = path.poses.size() - 5;
                Eigen::Vector3d closest_point;
                int min_ind = 0;

                for (int i = 0; i < size; i++){
                    if (i==0)
                        closest_point << path.poses[i].pose.position.x,
                                         path.poses[i].pose.position.y,
                                         path.poses[i].pose.position.z;
                    Eigen::Vector3d v_(path.poses[i].pose.position.x,
                                       path.poses[i].pose.position.y,
                                       path.poses[i].pose.position.z);
                    Eigen::Vector3d dis1_ = lio_pose.v - v_;
                    Eigen::Vector3d dis2_ = lio_pose.v - closest_point;
                    min_ind = i;
                    if (dis1_.norm() < dis2_.norm()){
                        closest_point = v_;
                    }
                }
                goal_ind = min_ind + 5;
            } else if (flag == 2){
                double t_ = ros::Time::now().toSec() - init_time;
                std::cout << "\ntime compare : " << t_ << " path : " << path.poses[0].header.stamp.toSec() << std::endl;
                for (int i = 0; i < path.poses.size(); i++){
                    goal_ind = i;
                    if (t_ < path.poses[i].header.stamp.toSec())
                        break;
                }
            }

            goal_pose.v << path.poses[goal_ind].pose.position.x,
                           path.poses[goal_ind].pose.position.y,
                           path.poses[goal_ind].pose.position.z;
            std::cout << "size of path : " << path.poses.size() << " and goal index : " << goal_ind << std::endl;

            Eigen::Vector3d err_v_;
            err_v_ = goal_pose.v - lio_pose.v;

            std::cout << "\nGoal : " << goal_pose.v.transpose() << std::endl;
            std::cout << "Pose : " << lio_pose.v.transpose() << std::endl;
            std::cout << "Err  : " << err_v_.transpose() << std::endl;

            float cmd_x, cmd_y;
            pid_compute(pid_x, cmd_x, err_v_.x(), err_v.point.x(), 0.001);
            pid_compute(pid_y, cmd_y, err_v_.y(), err_v.point.y(), 0.001);

            Eigen::Vector3d cmd(cmd_x, cmd_y, 0);//, euler(0, 0, ncrl_tf::Q2Euler(lio_pose.q).z());

            // trans imu_init frame cmd into body frame
            cmd = lio_pose.q.inverse() * cmd;

            // publish body frame's cmd
            vel_cmd.linear.x = cmd.x();
            vel_cmd.angular.z = cmd.y();

            if (ENABLE_FEEDFORWARD)
                vel_cmd.linear.x += path.poses[goal_ind].pose.orientation.x;

            // confine the cmd value
            if (fabs(vel_cmd.linear.x) > maxCmdX){
                vel_cmd.linear.x = vel_cmd.linear.x * maxCmdX / fabs(vel_cmd.linear.x);
            } else if (fabs(vel_cmd.linear.x) < minCmdX){
                vel_cmd.linear.x = 0;
            }

            if (fabs(vel_cmd.angular.z) > maxCmdW){
                vel_cmd.angular.z = vel_cmd.angular.z * maxCmdW / fabs(vel_cmd.angular.z);
            } else if (fabs(vel_cmd.angular.z) < minCmdW){
              vel_cmd.angular.z = 0;
            }

            pub_cmd.publish(vel_cmd);
            pub_path.publish(path);

            if (flag == 2){
                if (goal_ind > 0)
                    path.poses.erase(path.poses.begin(), path.poses.begin() + goal_ind - 1);
            }

            // create last term
            ncrl_tf::setPoint(err_v, err_v_);
        } else if (flag != 1){
            // no trajectory case
            Eigen::Vector3d err_v_;
            err_v_ = goal_pose.v - lio_pose.v;

            std::cout << "\nGoal : " << goal_pose.v.transpose() << std::endl;
            std::cout << "Pose : " << lio_pose.v.transpose() << std::endl;
            std::cout << "Err  : " << err_v_.transpose() << std::endl;

            float cmd_x, cmd_y;
            pid_compute(pid_x, cmd_x, err_v_.x(), err_v.point.x(), 0.001);
            pid_compute(pid_y, cmd_y, err_v_.y(), err_v.point.y(), 0.001);

            Eigen::Vector3d cmd(cmd_x, cmd_y, 0);//, euler(0, 0, ncrl_tf::Q2Euler(lio_pose.q).z());

            // trans imu_init frame cmd into body frame
            cmd = lio_pose.q.inverse() * cmd;

            // publish body frame's cmd
            vel_cmd.linear.x = cmd.x();
            vel_cmd.angular.z = cmd.y();
            if (fabs(vel_cmd.linear.x) <= 0.01)
                vel_cmd.linear.x = 0;
            if (fabs(vel_cmd.linear.y) <= 0.01)
                vel_cmd.linear.y = 0;

            pub_cmd.publish(vel_cmd);
        }

        std::cout << "linear velocity  : " << vel_cmd.linear.x <<
                     "\nangular velocity : " << vel_cmd.angular.z << std::endl;

        if (flag != 1){
            vel_cmd.linear.x = 0;
            vel_cmd.angular.z = 0;
        }

        //publish tf
        tf::Transform tf_pose, tf_goal;
        ncrl_tf::setTfTrans(tf_pose, lio_pose.q, lio_pose.v);
        br.sendTransform(tf::StampedTransform(tf_pose, ros::Time::now(), lio_pose.start_frame, lio_pose.end_frame));
        ncrl_tf::setTfTrans(tf_goal, goal_pose.q, goal_pose.v);
        br.sendTransform(tf::StampedTransform(tf_goal, ros::Time::now(), goal_pose.start_frame, goal_pose.end_frame));
        r.sleep();
    }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "husky_path");
    ros::NodeHandle nh;

    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
    bool init = readParameter(nh);

    // show variable
    if (init){
        std::cout << "\nHUSKY_PATH NODE : " <<
                     "\nPOS FEEDBACK TOPIC IS " << POS_topic <<
                     "\nMOTION PLANNING TOPIC IS " << Path_Topic <<  std::endl;

        std::cout << "\nPID CONTROL  : " <<
                     "\nX AXIS KP " << pid_x.KP << " KI " << pid_x.KI << " KD " << pid_x.KD <<
                     "\nY AXIS KP " << pid_y.KP << " KI " << pid_y.KI << " KD " << pid_y.KD << std::endl;

        std::cout << "\nfrequency : " << freq << " Hz" << std::endl;
    } else
        ros::shutdown();

    ros::Subscriber sub_pos = nh.subscribe<nav_msgs::Odometry> (POS_topic, 20, cb_pos);
    ros::Subscriber sub_path = nh.subscribe<nav_msgs::Path> (Path_Topic, 20, cb_path);
    //test
//    ros::Subscriber sub_pos_test = nh.subscribe<nav_msgs::Path> ("/estimator/pos_path", 20, cb_pos_test);


    pub_cmd = nh.advertise<geometry_msgs::Twist> ("/cmd_vel", 20);
    pub_path = nh.advertise<nav_msgs::Path> ("/future_path", 20);

    // initial setting
    ncrl_tf::setTransFrame(lio_pose, "WORLD", "IMU");
    ncrl_tf::setTransFrame(goal_pose, "WORLD", "GOAL");
    ncrl_tf::setPointFrame(err_v, "WORLD");
    ncrl_tf::setPointFrame(err_q, "WORLD");

    Eigen::Vector3d tmp_v(0,0,0), tmp_e(0,0,0);
    Eigen::Quaterniond tmp_q = ncrl_tf::Euler2Q(tmp_e);
    ncrl_tf::setTrans(lio_pose, tmp_q, tmp_v);
    ncrl_tf::setTrans(goal_pose, tmp_q, tmp_v);
    ncrl_tf::setPoint(err_v, tmp_v);
    ncrl_tf::setPoint(err_q, tmp_v);


    std::thread ctrl_process{process};
    ros::spin();
    return 0;
}
