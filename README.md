# ncrl_ugv_ctrl
---
## Target
* This package aims to control UGV with the current poses and the desired poses input.
* NOTE: The position error in x-axis will be transfer into the velocity command in X-axis and the orientation error and the position error in Y-axis will be transfer into the velocity command in Y-axis.

## setup
* Eigen
* ROS

## Author
* Chun-Jung Lin
* chadlin.gdr07g@nctu.edu.tw
